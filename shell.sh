#!/usr/bin/env bash

SELF=$(basename $0)
BINDIR=$(dirname $0)

source $BINDIR/.functions

DF="$HOME/.dotfiles/shell"


install () {
    uninstall
    pushd $DF
    for s in $(ls bash*) ; do
        _symlink_ $DF/$s ~/.$s
    done
    popd
}

uninstall () {
    pushd $DF
    for s in $(ls bash*) ; do
        if [ -f "$HOME/.$s" ] ; then
            _del_ $HOME/.$s
        fi
    done
    popd
}

cmd=$1 ; shift
case "$cmd" in
    install) $cmd ;;
    uninstall) $cmd ;;
    *) error 1 ;;
esac
