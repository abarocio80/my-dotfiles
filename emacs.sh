#!/usr/bin/env bash

SELF=$(basename $0)
BINDIR=$(dirname $0)

source $BINDIR/.functions

DF="$HOME/.dotfiles/emacs"
CD="${XDG_CONFIG_HOME:-$HOME/.config}"
PD="$HOME/Projects/code"

mkdir -p $CD
mkdir -p $DF/$HOSTNAME
mkdir -p $PD

install () {
    pushd $DF

    if [ ! -f $HOSTNAME/projects ] ; then
        if [ -f "$CD/crafted-emacs/var/projects" ] ; then
            cp crafted-emacs-config/var/projects $HOSTNAME/projects
        else
	    touch $HOSTNAME/projects
        fi
    fi
    git add -f $HOSTNAME/ 2>/dev/null

    if [ ! -d "chemacs2" ] ; then
        _clone_repo_ https://github.com/plexus/chemacs2.git
    fi
    _del_ $CD/emacs
    _symlink_ $DF/chemacs2 $CD/emacs

    if [ ! -d "crafted-emacs" ] ; then
        _clone_repo_ https://github.com/SystemCrafters/crafted-emacs.git
    fi

    _del_ vanilla
    mkdir -p vanilla
    if [ -f "$DF/$HOSTNAME/projects" ] ; then
        _symlink_ $HOSTNAME/projects vanilla/projects
    fi

    if [ ! -d "crafted-emacs-config" ] ; then
        _clone_repo_ git@gitlab.com:abarocio80/crafted-emacs-config.git
        mkdir -p crafted-emacs-config/var
        if [[ -f "$DF/$HOSTNAME/projects" \
                  && ! crafted-emacs-config/var/projects ]] ; then
            _symlink_ $DF/$HOSTNAME/projects crafted-emacs-config/var/projects
        fi
    fi

    if [ ! -d "simple-emacs" ] ; then
        _clone_repo_ git@gitlab.com:abarocio80/simple-emacs.git
        if [ -f "$DF/$HOSTNAME/projects" ] ; then
            _symlink_ $DF/$HOSTNAME/projects simple-emacs/projects
        fi
    fi

    _del_ "$CD/chemacs"
    mkdir -p "$CD/chemacs"
    _symlink_ $DF/profiles.el $CD/chemacs/profiles.el
    _symlink_ $DF/profile $CD/chemacs/profile
    popd

    pushd $PD
    if [ ! -d "crafted-emacs" ] ; then
        _clone_repo_ git@github.com:abarocio80/crafted-emacs.git
    fi
    if [ ! -d "org-config.el" ] ; then
        _clone_repo_ git@gitlab.com:abarocio80/org-config.el.git
    fi
    if [ ! -d "teacher.el" ] ; then
        _clone_repo_ git@gitlab.com:abarocio80/teacher.el.git
    fi
    popd
    _del_ $HOME/.emacs.d
    emacs -f "all-the-icons-install-fonts"
}

uninstall () {
    _del_ $DF/chemacs2
    _del_ $DF/crafted-emacs
    _del_ $DF/crafted-emacs-config
    _del_ $DF/simple-emacs
    _del_ $DF/vanilla
    _del_ $CD/emacs
    _del_ $CD/chemacs
    _del_ $PD/crafted-emacs
    _del_ $PD/org-config.el
    _del_ $PD/teacher.el
}

cmd=$1 ; shift
case "$cmd" in
    install) $cmd ;;
    uninstall) $cmd ;;
    *) error 1 ;;
esac
